import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import {ActivatedRoute,Router} from '@angular/router';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {EmpDataService} from './emp-data.service';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {
  titleText="Edit Employee";
  loc=["Bangalore", "Chennai", "Pune", "Hyderabad"];
  constructor(private router:Router, private empSer:EmpDataService, private route:ActivatedRoute, private title:Title) { }
  emp:Employee;
  id: number;
  name: string;
  location: string;
  email: string;
  mobile: string;
  editDetails;
  ngOnInit() {
    this.title.setTitle(this.titleText);
    this.id=this.route.snapshot.params['id'];
    this.getEmployee();
    this.validateForm("","","","");
    
  }
  public getEmployee():void {
    this.empSer.getEmployee(this.id).subscribe(emp=>{
      this.emp=emp;
      this.name=emp.name;
      this.location=emp.location;
      this.email=emp.email;
      this.mobile=emp.mobile;
      this.validateForm(this.name,this.location,this.email,this.mobile);
    });
  }
  public onUpdate(empl :FormGroup):void {
    this.emp.name=empl.name1;
    this.emp.location=empl.location1;
    this.emp.email=empl.email1;
    this.emp.mobile=empl.mobile1;
    this.updateDetails();
  }
  public updateDetails() {
    this.empSer.updateEmployee(this.emp).subscribe(emp=>{});
    this.router.navigate(["/employees-list"]);
  }
  public validateForm(name,loc,email,mob) {
    this.editDetails=new FormGroup({
      name1: new FormControl(name, Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern('^[a-zA-Z ]*$')])),
      location1: new FormControl(loc, Validators.compose([Validators.required,Validators.minLength(3),Validators.pattern('^[a-zA-Z ]*$') ])),
      email1: new FormControl(email, Validators.compose([Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")])),
      mobile1: new FormControl(mob, Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10),Validators.pattern("[0-9]*")]))
    });
  }
}