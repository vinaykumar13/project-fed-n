import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EmpDataService} from './emp-data.service';
import { Employee} from './employee';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
  titleText="Employee Details";
  id:number;
  constructor(private router:Router, private empSer:EmpDataService,private route:ActivatedRoute, private title:Title) { }
  emp:Employee;
  ngOnInit() {
    this.title.setTitle(this.titleText);
    this.route.params.subscribe(paramsId=>{
      
      this.id=paramsId.id;
    });
    this.getEmployee();
  }
  public getEmployee():void {
    this.empSer.getEmployee(this.id).subscribe(employee=> {
      this.emp=employee
    });
  }
  public nav_home() {
    this.router.navigate(["/employees-list"]);
  }

}