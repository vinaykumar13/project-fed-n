import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import { EmpDataService } from './emp-data.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  titleText="Add Employee";
  emp: Employee[]=[];
  userDetails;
  name;
  location;
  email;
  mobile;
  a;
  loc=["Bangalore", "Chennai", "Pune", "Hyderabad"];
  emp1:Employee[];
  constructor(private empService: EmpDataService, private router: Router, private title:Title) { }

  ngOnInit() {
    this.title.setTitle(this.titleText);
    this.userDetails=new FormGroup({
      name: new FormControl("", Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern('^[a-zA-Z ]*$')])),
      location: new FormControl("", Validators.compose([Validators.required,Validators.minLength(3),Validators.pattern('^[a-zA-Z ]*$') ])),
      email: new FormControl("", Validators.compose([Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$')])),
      mobile: new FormControl("", Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("[0-9]*")]))
    });
  
  }
  onSubmit(employee): void {
    this.a =this.empService.addEmployee(employee);
    this.empService.addEmployee(employee).subscribe(employee=>{
      this.emp.push(employee)
    });
    this.router.navigate(["/employees-list"]);
  }

}