import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Employee } from './employee';


@Injectable({
  providedIn : 'root'
})
export class EmpDataService {
  ser_url="./employees";
  httpOptions={
    headers:new HttpHeaders({'content-type':'application/json'})
  };
  constructor(private hc:HttpClient) {}
  public getEmployees():Observable<Employee[]> {
    return this.hc.get<Employee[]>(this.ser_url)
  }
  public addEmployee(employee:Employee): Observable<Employee>{
    return this.hc.post<Employee>(this.ser_url,employee,this.httpOptions);
  }
  public updateEmployee(employee:Employee): Observable<any> {
    return this.hc.put(this.ser_url,employee,this.httpOptions)
  }
  public deleteEmployee(employee: Employee|number): Observable<Employee> {
    const id=employee;
    const url= `${this.ser_url}/${id}`;
    return this.hc.delete<Employee>(url,this.httpOptions)
  }
  public getEmployee(id:number):Observable<Employee> {
    const url=`${this.ser_url}/${id}`;
    return this.hc.get<Employee>(url)
  }
}