import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { EmployeesListComponent } from './employees-list.component';
import {AddEmployeeComponent} from './add-employee.component';
import { InMemoryService } from './in-memory.service';
import { EmpDataService } from './emp-data.service';
import { HttpClientModule } from '@angular/common/http';
import { FilterSearchPipe } from './filter-search.pipe';
import { FormsModule} from '@angular/forms';
import { DeleteEmployeeComponent } from './delete-employee.component';
import { EditEmployeeComponent } from './edit-employee.component';
import { EmployeeDetailsComponent } from './employee-details.component';
import { ErrorPathComponent } from './error-path.component';
@NgModule({
  imports:      [ BrowserModule, HttpClientModule,FormsModule, ReactiveFormsModule, AppRoutingModule, InMemoryWebApiModule.forRoot(InMemoryService) ],
  declarations: [ AppComponent, EmployeesListComponent, AddEmployeeComponent, FilterSearchPipe, DeleteEmployeeComponent, EditEmployeeComponent, EmployeeDetailsComponent, ErrorPathComponent],
  bootstrap:    [ AppComponent ],
  providers: [InMemoryService, EmpDataService]
})
export class AppModule { }
