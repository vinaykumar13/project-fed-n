import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-error-path',
  template: '<h1><font color="red">Invalid Path</font><h1>'
})
export class ErrorPathComponent implements OnInit {

  constructor(private router: Router) { }
  
  ngOnInit() {
    
  }

}