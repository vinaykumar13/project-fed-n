import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from './employee';
@Pipe({
  name: 'filterSearch'
})
export class FilterSearchPipe implements PipeTransform {

  transform(employees: Employee[], search?: String): Employee[] {
    if(!employees || !search) {
      return employees;
    }
    return employees.filter(employee=>employee.name.toLowerCase().startsWith(search.toLowerCase()));
  }

}